{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# *Decision Tree* classification of Weather Observation Data\n",
    "\n",
    "Determine whether **Decision Tree Learning** can predict the **type** of weather *given* the available ground observations. You will be using the same dataset as in Unit 5 (observation data from **June** across all UK Met Office weather stations).\n",
    "\n",
    "Tasks: \n",
    "- Clean and transform the data for use by Scikit-learn  \n",
    "- Create a training and testing sample \n",
    "- Run a decision tree classification process and evaluate predictive power \n",
    "- Investigate prediction accuracy changes with hyper-parameter tuning \n",
    "- Elaborate on any limitations and future improvements \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notes on the assessment \n",
    "- Please follow the tasks provided \n",
    "- All **10** tasks are worth **1** mark each\n",
    "- Code must be understandable and reproducible. Before grading the notebook kernel will be **restarted** and **re-run**\n",
    "- If you are unsure on how to proceed please reference the *Iris dataset* decision tree notebook for relevant examples\n",
    "- Please ask one of the TAs if you are really stuck!\n",
    "- If you are unable to complete the question highlight what approaches you should take to extract the data and result of interest\n",
    "- Notebooks will be graded from now until **Monday 5th November (5pm)** "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notes on the setup and notebook\n",
    "- This notebook will **not** work on the Lab PCs machines by default and will need to be launched within a virtual environment with all the packages installed. See `setup-instructions.md` for more details \n",
    "- Successful installation is not part of the assessment so please ask one of the teaching assistants for help if you have any problems running the code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from sklearn import datasets, tree, metrics, model_selection, ensemble\n",
    "from sklearn.externals.six import StringIO \n",
    "from IPython.display import Image\n",
    "from mpl_toolkits.mplot3d import Axes3D\n",
    "import pydotplus\n",
    "import itertools\n",
    "import seaborn as sns\n",
    "import dt_utils\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read CSV file: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "obs = pd.read_csv('obs.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check that the data is read in correctly: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "obs.head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should have **106,553** observations with **17** features per observation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "obs.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we define four **features** of interest to be used as training in the decision tree. I have chosen *Wind Speed*, *Temperature*, *Humidity* and *Elevation*.\n",
    "\n",
    "The target output (`Type`) and the three weather type classes (*Clear*, *Cloudy* and *Precip* (precipitation)) are also defined:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define data and features\n",
    "features = ['WindSpeed', 'Temperature', 'Humidity', 'Elevation']\n",
    "output = ['Type']\n",
    "wtype = ['Clear', 'Cloudy', 'Precip']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1. Create a *derived* dataset of the imported observation data that satisfies the following requirements\n",
    "- Has 5 columns containing observations of the four features of interest defined above and the weather type (`Type`) \n",
    "- All the **duplicate rows** have been removed\n",
    "- Any rows with **null values** have been removed \n",
    "- Any rows with `Type` of value **3** (*unrecorded*) have been removed  \n",
    "\n",
    "Refer to the Unit 5 notebooks on how apply the above conditions to a Pandas dataframe. After filtering you should have **87,496** rows remaining"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you run `dataset.groupby(dataset.Type).count()` (or similar) you should see that there is a much lower sample size for one of the types in your dataset. In order to avoid **class imbalance** (which would lead to an inccurate decision tree) we should attempt to get a roughly equal number of observations per weather type. Lets first run with a modest sample size and prepare the data in the form expected for a *scikit-learn* estimator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2. Take the first 50 observations for each weather type, combine the observations and create two arrays (`data` and `target`) that contain the following:\n",
    "- `data` is a (150 x 4) array containing all the observation data \n",
    "- `target` is a (150 x1) array containing all the weather types \n",
    "\n",
    "Hints: \n",
    "- You could use the `pd.concat` method (not covered in Unit 5) to combine DataFrames that have the identical feature columns\n",
    "- Data can be extracted from a Pandas DataFrame using the `values` method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 3. Generate 6 x 2D plots showing the weather type distribution of our selected 150 observations\n",
    "- Use one of the utility methods in `dt_utils` to plot the distributions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 4.  Construct and train a decision tree to classify weather type based on your training data with the following requirements: \n",
    "- 70% of the observations are reserved for training\n",
    "- Tree depth is limited to 5 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 5. Using the provided utility functions in `dt_utils` show the following: \n",
    "- The decision tree logic \n",
    "- The decision surfaces for all the input features\n",
    "- Ensure that the plot step on each surface is greater than 0.5 to avoid memory errors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 6. Display a classification report and confusion matrix for your predictions. How many false positives are observed for clear conditions?  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should see that the results are suprisingly good with high precision and accuracy (~90%). At this point we have only selected a small sample of the available data and the results seem suspiciously high. Lets see how good performance is with the largest sample of balanced data available. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 7. Now repeat the decision tree classification and performance assessment above with the following:\n",
    "- **3,500** observations per weather type (**10,500** observations in total)\n",
    "- **80%** of the data used for training the decision tree \n",
    "- A maximum tree depth of 4 \n",
    "- A minium samples split of 50 \n",
    "- Do **not** repeat the decision surface plots (too many data points)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should that the performance is not as good but much better than chance (33%). We can now try and improve the result by performing a grid search to choose the best hyper-parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 8. Perform a grid-search on **two** suitable hyper-parameters of your own choosing across a 8x8 grid to find better accuracy\n",
    "- Determine the best combination of hyper-parameters\n",
    "- Re-apply the decision tree fit and evaluate performance \n",
    "\n",
    "Notes: \n",
    "- The distance between successive points does not have to be uniform\n",
    "- Default to applying the grid-search on the **training** data (though you can apply it to whole 10,500 observations for comparision if curious)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets now consider how we could improve performance in a future iteration of the analysis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 9. Briefly describe what further steps you can try that could help improve testing accuracy\n",
    "- Were there any biases or limitations on the above steps? \n",
    "- How could the quality of the dataset be improved?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 10. Illustrate a *derived* feature from the available data that could be used as a discriminating feature for determining weather types \n",
    "- Do not attempt to code this new feature and apply to a decision tree. Just provide some justification on why you think this would be good to attempt in a future analysis\n",
    "- There is no wrong answer here as long you can provide some reasoning\n",
    "- Try and apply your reasoning based on real weather patterns - though you are not expected to be a meteorologist! "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
