import numpy as np
from sklearn import datasets, tree
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz
import pydotplus
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
import tensorflow as tf

