#!/bin/bash


# Python 2 
virtualenv -p python2 /scratch/pdaml-env
source /scratch/pdaml-env/bin/activate
pip install --upgrade pip
pip install pandas matplotlib scipy sklearn pydotplus seaborn tensorflow ipython ipykernel 
ipython kernel install --user --name=pdaml-env
deactivate

