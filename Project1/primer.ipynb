{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Census Data Analysis Primer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook will introduce you to the tools available to access, extract and visualise the **Scotland Census 2011 data**. Before starting your own analysis run the examples detailed below so that you get a good understanding of the  methods available. \n",
    "\n",
    "The notebook must first be moved into your project folder `census-UUN` (where `UUN` is your ID) before running any of the examples.  \n",
    "\n",
    "Once you have run the examples in this notebook create a new notebook in your project folder named `census-UUN.ipynb` which will contain your own analysis and commentary. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import census_utils\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ensure descriptions are not truncated \n",
    "pd.set_option('display.max_colwidth', -1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Census Lookup table \n",
    "\n",
    "The file `lookuptable.pkl` contains the full list of table names and categories as a Pandas dataframe. Use the lookup table to explore which tables and categories are of interest in your study.\n",
    "\n",
    "The lookup pickle file is available [here](https://www2.ph.ed.ac.uk/~awashbro/PDAML/lookup/lookuptable.pkl). Before running the cell below download the file and move it into your `lookup/` directory.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get lookup table from https://www2.ph.ed.ac.uk/~awashbro/PDAML/lookup/lookuptable.pkl and store locally before running\n",
    "lookup = pd.read_pickle('../lookup/lookuptable.pkl') "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lookup.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can apply filters on the `lookup` dataframe to explore the different tables available and the associated categories in each table. You will need to reference the `UniqueCatCode` associated with each entry when extracting the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# filter on table code\n",
    "lookup[lookup.TableCode == 'LC6109SC'].head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Extracting Feature Data\n",
    "\n",
    "Once you have selected some features of interest use `extractdata` to get the data supplying: \n",
    "\n",
    "- `requested`: The selected codes *as a dictionary* (using the table code as the key)\n",
    "- `norm`: An *optional* **normalisation** mapping *as a dictionary* (see project guide for details) \n",
    "- `geo`: An *optional* geography type  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following example I chose the following:   \n",
    "1. \"Accommodation type by tenure - Households\" / \"All households_Owned: Owned outright\"\n",
    "2. \"Dependent children by household type by age\" / \"All people aged 16 and over in households_Living in a couple household: Dependent children in household\"\n",
    "3. \"Hours worked by sex by age\" / \"All people aged 16 to 74 in employment: Total_Full-time: Total\"\n",
    "  \n",
    "Then I normalised the data with:\n",
    "1. All households\n",
    "2. Living in a couple household: Total\n",
    "3. Total All people aged 16 to 74 in employment  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "requested = { 'LC4427SC': ['0003'],\n",
    "              'LC1112SC': ['0006'],\n",
    "              'LC6109SC': ['0005']\n",
    "            }\n",
    "norm = { 'LC4427SC' : { '0003':'0001' },\n",
    "         'LC1112SC' : { '0006':'0004' },\n",
    "         'LC6109SC' : { '0005':'0001' }\n",
    "       }\n",
    "\n",
    "features = census_utils.extractdata(requested, norm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All features are now normalised and contained in a dataframe ready for analysis:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "features.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "features.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The default census **geography** is *LC Postcode Sector*. You can change this if you wish by applying a new geography type (`geo`) to `extractdata`. Unless you have a specific study in mind I would recommend sticking to the default. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Example using different geography\n",
    "# geo = \"Scottish Parliamentary Constituency\"\n",
    "# requested = { 'LC4427SC': ['0003'],\n",
    "#               'LC1112SC': ['0006'],\n",
    "#               'LC6109SC': ['0005']\n",
    "#             }\n",
    "# norm = { 'LC4427SC' : { '0003':'0001' },\n",
    "#          'LC1112SC' : { '0006':'0004' },\n",
    "#          'LC6109SC' : { '0005':'0001' }\n",
    "#        }\n",
    "# features = extractdata(requested, norm, geo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Choropleth Maps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some of the trends of your chosen features may be regional (i.e north vs south Scotland, city areas vs rural areas). You can show variations between areas on a map of Scotland with a colour palette allowing any regional variations. These are known as [choropleth maps](https://en.wikipedia.org/wiki/Choropleth_map). For this study `folium` was chosen as this allows zooming in and out to explore detail in built up areas as well as the use of Openstreetmap tiling to display place names underneath the shading. \n",
    "\n",
    "A utility function `genchoropleth` makes it simple for you to generate these maps or can call the `folium` method directly. Before running the cell below download the map file `LC.geojson` from [here](https://www2.ph.ed.ac.uk/~awashbro/PDAML/maps/LC.geojson) and move it into your `maps/` directory.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# NOTE - before running make sure you have downloaded LC.geojson into your maps directory \n",
    "census_utils.genchoropleth(features, 'LC6109SC0005')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can apply your own map customisations by passing `map` and `choropleth` arguments as a dictionary to `genchoropleth`. For a full list of options see the `folium.map` and `folium.Choropleth` docstrings and the [folium documentation](http://python-visualization.github.io/folium/docs-v0.5.0/modules.html).\n",
    "\n",
    "In the example below I have changed the colour palette to *yellow-green*, added a legend name and positioned to map to centre on Edinburgh: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# customised map focus on Edinburgh\n",
    "copts = { \"fill_color\" : \"YlGn\", \"legend_name\" : \"Proportion of Household owners\"}\n",
    "mopts = { \"location\" : [55.95, -3.19], \"zoom_start\" : 11}\n",
    "census_utils.genchoropleth(features, 'LC4427SC0003', mopts_custom=mopts, copts_custom=copts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generating a classification target "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The majority of the data is unlabelled and is therefore more suited to a regression study (i.e predicting the *value* of the target for a given area). You can either perform a regression using appropriate machine learning methods or segment the target data into classes.\n",
    "\n",
    "In this first example I will use the proportion of the population in each area considered in *very good* health as a target feature. This data is found in table LC3102SC with unique code LC3102SC0002. To generate a proportion I will normalise this against LC3102SC0001 (*all* people in the area).\n",
    "\n",
    "**Tiers** are defined to segment the data into a set number of classes. In the example below I have defined 3 classes: \n",
    "\n",
    "- Class 1: below the 20% quantile \n",
    "- Class 2: between the 20% and 80% quantiles \n",
    "- Class 3: above the 80% quantile   \n",
    "\n",
    "This information is then passed to the `genclfsingle` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# single feature classification \n",
    "feature = 'LC3102SC0002'\n",
    "norm = 'LC3102SC0001' \n",
    "tiers = [0.2, 0.8]\n",
    "outcome = census_utils.genclfsingle(feature, tiers, norm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`genclfsingle` then returns a dataframe with the `Class` as a column:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "outcome.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see the distribution of the `Class` values using the `Series` `hist` method: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "outcome.Class.hist()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should see that the values are distributed as defined by the quantile value ranges"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Combining multiple features"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In some cases it may be more useful to define a target feature that is composed of multiple census data features. This is possible using the `genclfscore` method.\n",
    "\n",
    "In the second example below I take the following values from table LC3102SC (General health by age): \n",
    "\n",
    "- LC3102SC0002: Very good health\n",
    "- LC3102SC0003: Good health\n",
    "- LC3102SC0004: Fair health\n",
    "- LC3102SC0005: Bad health\n",
    "- LC3102SC0006: Very bad health\n",
    "\n",
    "The values for each are weighted for each area (*Very good* = 5 to *Very bad* = 1). A weighted average is then calculated for each area such that a metric is generated between 1-5 that captures the health of the population in a given area."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# multi-feature classifier \n",
    "requested = { 'LC3102SC': ['0002', '0003', '0004', '0005', '0006'] }\n",
    "weights = { 'LC3102SC0002' : 5, \n",
    "           'LC3102SC0003' : 4,\n",
    "           'LC3102SC0004' : 3,\n",
    "           'LC3102SC0005' : 2,\n",
    "           'LC3102SC0006' : 1 }\n",
    "tiers = [0.2, 0.8]\n",
    "outcome = census_utils.genclfscore(requested, weights, tiers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`genclfscore` then returns a dataframe with the `Score` and `Class` as columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "outcome.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see the distribution of both `Score` and `Class` using the `Series` `hist` method: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "outcome.Class.hist()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "outcome.Score.hist()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally lets visualise the health score and health score class using `genchoropleth`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "census_utils.genchoropleth(outcome, 'Score')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "census_utils.genchoropleth(outcome, 'Class')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A naive study shows that Aberdeenshire is a very healthy place to live and that you should live on the west side of Edinburgh! If this was the basis for your study you would then look to explain the result by considering the factors that contribute to (and predict) general health within an area. You would then use selected features to try and predict health by using a chosen machine learning technique. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
