{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using Pandas to explore the *Countries of the world* dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook provides examples on how to use **Pandas** for the analysis of a sample dataset. Here we will be using the **countries of the world** dataset for illustration. \n",
    "\n",
    "The source data has been made available in the course Git repository and is also available via the [Kaggle Datasets page](https://www.kaggle.com/fernandol/countries-of-the-world/home).\n",
    "\n",
    "Please note that this notebook is not intended to be a complete Pandas tutorial! Please visit the [Pandas webpage](http://pandas.pydata.org/) for a reference on how to use the methods described below and more in-depth examples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note for running standalone Python version**\n",
    "- In order to see the output of any of the steps enclose the statement with a `print` statement (e.g `c.head(3)` to `print(c.head(3)`).\n",
    "- Plots should be rendered automatically. To supress plots comment the  `plt.show` or `ax.plot` where appropiate\n",
    "\n",
    "**Running in Python 2**\n",
    "- The notebook was written in Python 3 but should (hopefully) just work in Python 2. Uncomment the line below if you are using the `print` syntax from Python 3 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# from __future__ import print_function # print function from Python3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import SciPy Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy import stats\n",
    "%matplotlib inline # comment this in standalone version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data Cleaning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The source dataset is provided in CSV format so no data preparation is needed before we import the file into a Pandas DataFrame. The `read_csv` method takes the data from file and constructs a new `DataFrame`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# read CSV file\n",
    "c = pd.read_csv('countries.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we start any analysis lets first inspect the data to see if it has been read correctly and makes sense. To view a small snippet of the data frame use the `head` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To see how many rows and columns the dataframe holds use the `shape` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So there are in total 227 countries listed and 20 columns (or features).\n",
    "\n",
    "The data seems to have been read correctly at first glance but on closer inpection there are commas instead of decimal places in some of the columns. This can be fixed with the `decimal` parameter in the `read_csv` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# read CSV file again\n",
    "c = pd.read_csv('countries.csv', decimal=\",\")\n",
    "c.head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets now look at the column names:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.columns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As part of the data exploration we will continually reference the column names during filtering and querying. The imported column names have extraneous characters (e.g. `%`) which will be inconvenient to reference in the code so it is worthwhile at this stage to simplify the columns before we perform any analysis steps: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.columns = ['Country', 'Region', 'Population', 'Area', 'Density', 'Coastline', 'Migration', 'Infant mortality', \\\n",
    "             'GDP', 'Literacy', 'Phones', 'Arable', 'Crops', 'OtherLand', 'Climate', 'Birthrate', 'Deathrate', \\\n",
    "             'Agriculture', 'Industry', 'Service']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some care will also needed with using string data when applying pattern matches. First consider values in the `Region` column. To select a single column from the DataFrame use the following notation:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.Region.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(c.Region)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This provides all the entries of the column in an object of type `Series`\n",
    "\n",
    "Here we are only interested in the unique entries in the column. This can be provided by the `unique` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.Region.unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see lots of spaces at the end of each entry. These can be removed using the `strip` method in `str`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.Region = c.Region.str.strip()\n",
    "c.Region.unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have stripped the whitespace in each entry and assigned this back to the original DataFrame. \n",
    "\n",
    "Similarly in the `Country` colmnn there is an extra space at the end of each value that needs to be stripped:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.Country.head(5).tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.Country = c.Country.str.strip()\n",
    "c.Country.head(5).tolist()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we have used `tolist` to change the format from a `Series` to a python `list` object.\n",
    "\n",
    "Alternatively, a strip of **all** string columns in the DataFrame can be done using a `lambda` expression in the `applymap` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = c.applymap(lambda x: x.strip() if type(x) is str else x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we proceed to data exploration let us check whether all the entries have meaningful data in each of the columns: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.count()\n",
    "#c.info() # alternative"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From `count` and `info` we can see that not all of the 227 counties have entries in each column. For example, 22 countries have missing climate data.\n",
    "\n",
    "It is important to perform this step when conducting an analysis over features with missing data. The `dropna` method is used to remove any null entries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lit = c.Literacy.dropna()\n",
    "lit.shape # now has 209 entries (none of type Null)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see from `shape` that the `Series` now has only 209 entries instead of 227. \n",
    "\n",
    "Let us look at the structure of the `DataFrame` again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The index is on the left (0,1,2) is meaningless for any analysis and it would be best to change the index to `Country`. This can be done with `set_index`:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.set_index('Country', inplace=True)\n",
    "c.head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our source data has now been sanitised!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Extraction and Slicing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To extract selected rows from a large dataset use `iloc` to slice by indices: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.iloc[2:5] \n",
    "#c[2:5] # alternative "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To find a specific country in the index use `loc`: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.loc['Belgium']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A list can also be provided to `loc`: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "countries = ['Belgium', 'France', 'Spain']\n",
    "c.loc[countries]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Slicing can be combined with column selection:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c[2:5].Population"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This generates a `Series` object. To generate a NumPy `array` or python `list` use the following methods:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c[2:5].Population.values # numpy array\n",
    "c[2:5].Population.tolist() # python list "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To display multiple columns:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "features = ['Population', 'GDP']\n",
    "c[features].head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Querying"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Entries can be filtered by applying query statements.\n",
    "\n",
    "For example, lets select countries in the Baltic region:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c[c.Region == 'BALTICS'] \n",
    "# c.query('Region == \"BALTICS\"') # alternative using query() \n",
    "# c.Region.str.match('BALTICS') # alternative using str.match()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This can be combined with selecting columns using `loc`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "features = ['Population', 'GDP']\n",
    "c.loc[c.Region == 'BALTICS', features]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The filter itself is a `Series` of boolean values that can be applied as a mask to the `DataFrame`: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qry = c.Region == 'BALTICS'\n",
    "type(qry)\n",
    "# qry # print to see True/False identities based on condition test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Series` methods can also applied as filters. For example, instead of looking for an exact string match we may wish to query whether a `Region` is one of a selected list: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "easteurope = ['BALTICS', 'EASTERN EUROPE'] \n",
    "features = ['Region', 'Population', 'GDP']\n",
    "c.loc[c.Region.isin(easteurope), features]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More complex queries can be constructed using boolean operators. For example lets extract data from countries in Eastern Europe and the Baltics have a population of less than 3 million: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qry = (c.Region.isin(easteurope)) & (c.Population < 3000000) \n",
    "c[qry]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generating Summary Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We would now like to start providing summary statistics for extracted data. Lets start by counting the number of countries in each region:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# display value counts of Region\n",
    "c.Region.value_counts()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`sort_values()` can then order the results in ascending or descending order:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.Region.value_counts().sort_values() # sort by ascending \n",
    "# c.Region.value_counts().sort_values(ascending=False) # sort by descending"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`sort_values` can also be applied to multiple columns with the sort field chosen using the `by` parameter.\n",
    "\n",
    "For example, if we wanted to display data from countries which had the highest GDP:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.sort_values(by='GDP', ascending=False).head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Summary statistics can be generated from groups of data using the `groupby` method. For example to extract the mean birth rate per geographical region: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.groupby('Region').Birthrate.mean().sort_values(ascending=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The long chain of method calls above can be split into individual calls if this improves clarity:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "region = c.groupby('Region')\n",
    "birthrate = region.Birthrate\n",
    "meanbirth = birthrate.mean()\n",
    "meanbirth.sort_values(ascending=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`describe` and `agg` are helper methods for `Series` and `DataFrame` objects to provide useful summary statistics:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.groupby('Region').Birthrate.describe()\n",
    "# c.groupby('Region').Birthrate.agg(['count', 'max','mean']) # select subset of summary data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Appending Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may also wish to add new features to our data based on entries in other columns. \n",
    "\n",
    "In this example we classify population values into five categories:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ptype = []\n",
    "for p in c.Population: \n",
    "    p /= 1000000\n",
    "    if (p < 1):\n",
    "        ptype.append('tiny')\n",
    "    elif ((p >= 1) and (p < 10)):\n",
    "        ptype.append('small')\n",
    "    elif ((p >= 10) and (p < 100)):\n",
    "        ptype.append('medium') \n",
    "    elif ((p >= 100) and (p < 1000)):\n",
    "        ptype.append('large')\n",
    "    elif (p >= 1000):\n",
    "        ptype.append('huge')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c['PopulationType'] = pd.Series(ptype, index=c.index)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "features = ['Population', 'PopulationType']\n",
    "c[features].sort_values(by='Population', ascending=False).head(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Iterating over Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In an analysis we might wish to apply operations per row using multiple features. `iterrows` will provide each row in the DataFrame as an iterator than can be used in a `for` loop. \n",
    "\n",
    "A simple example below generates the Population/GDP ratio for each country:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ratio = {}\n",
    "for index, row in c.iterrows():\n",
    "    ratio[index] = row['Population'] / row['GDP']\n",
    "ratio['Spain']    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualisation "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we can query and extract any portion of our dataset we can create visualisations using `matplotlib`.\n",
    "\n",
    "`Series` object types can recognised by `matplotlib` and do not have to be converted beforehand. We can therefore plot a histogram of the distribution of values from the columns in just a few steps.\n",
    "\n",
    "As an example lets look at how land is used in each country: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#plt.hist(c.Arable, bins=50, range=(0,100)) # this will not work!\n",
    "plt.hist(c.Arable.dropna(), bins=50, range=(0,100)) # null value clean\n",
    "plt.title(\"Arable land\")\n",
    "plt.xlabel(\"Proportion (%)\")\n",
    "plt.ylabel(\"Count\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here you see the `dropna()` method was applied to the Arable series to avoid problems with `Null` entries. \n",
    "\n",
    "Side by side comprarison plots can also be generated in the same way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO - better orientation\n",
    "plt.figure(figsize=(12.0, 3.0))\n",
    "\n",
    "plt.subplot(131)\n",
    "plt.hist(c.Arable.dropna(), bins=50, range=(0,100))\n",
    "plt.title(\"Arable\")\n",
    "plt.xlabel(\"Proportion (%)\")\n",
    "plt.ylabel(\"Count\")\n",
    "\n",
    "plt.subplot(132)\n",
    "plt.hist(c.Crops.dropna(), bins=50, range=(0,100))\n",
    "plt.title(\"Crops\")\n",
    "plt.xlabel(\"Proportion (%)\")\n",
    "plt.ylabel(\"Count\")\n",
    "\n",
    "plt.subplot(133)\n",
    "plt.hist(c.OtherLand.dropna(), bins=50, range=(0,100))\n",
    "plt.title(\"Other Land\")\n",
    "plt.xlabel(\"Proportion (%)\")\n",
    "plt.ylabel(\"Count\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also generate the same plots by applying `plot` and `hist` methods directly to a `Series`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# alternative \n",
    "ax = c.Arable.dropna().hist(bins=50, range=(0,100), grid=False)\n",
    "ax.set_title('Arable')\n",
    "ax.set_xlabel('Proportion (%)')\n",
    "ax.set_ylabel('Count')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For generating multiple plots:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.loc[c.Region == 'EASTERN EUROPE', ['Birthrate', 'Literacy']]\\\n",
    ".dropna().hist(bins=10, grid=False, color='r', alpha=0.5)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`plot` can be used to access other types of plots - such as a horizontal bar plot.\n",
    "\n",
    "Here we show the top 10 countries with the highest phone ownership:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.Phones.sort_values(ascending=False).head(10).plot(kind='barh')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`plot` can be applied to a `DataFrame` object to generate scatter plots.\n",
    "\n",
    "For example, lets use a scatter plot to investigate the relationship between phone ownership and GDP:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c.plot.scatter(x='Phones', y='GDP', color='black')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By visual inspection there appears to be a correlation between these two features - so lets perform a simple linear regression to determine the strength of the relationship:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create new dataframe with features of interest\n",
    "d = c[['Phones', 'GDP']].dropna()\n",
    "\n",
    "# perform linear regression\n",
    "gradient,intercept,r_value,p_value,std_err=stats.linregress(d.Phones, d.GDP)\n",
    "\n",
    "print(\"regression results:\")\n",
    "print(\" gradient %f\\n intercept: %f\\n r_value: %f\\n \\\n",
    "p_value %f\\n std_err %f\\n\" % \\\n",
    "      (gradient,intercept,r_value,p_value,std_err))\n",
    "\n",
    "# plot scatter\n",
    "c.plot.scatter(x='Phones', y='GDP', color='black')\n",
    "\n",
    "# plot best fit line\n",
    "X = list(range(0,int(d.Phones.max()),100)) # number of points is arbitrary\n",
    "yfit = [intercept + gradient * xi for xi in X]\n",
    "plt.plot(X, yfit, color='red', linestyle='dashed', linewidth=3)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The r-value suggests that phone ownership is a good predictor of GDP - but does correlation mean causation? I will leave that up to you to decide! "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
