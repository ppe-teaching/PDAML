{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Preprocessing Countries of the World Data for Machine Learning Analysis  \n",
    "\n",
    "Some machine learning algorithms - such as Artifical Neural Networks - are sensitive to **feature scaling**. Input features with variances with orders of magnitude difference can impair classification power. Furthermore, any labelled data used as input needs to enumerated. \n",
    "\n",
    "It is therefore necessary to perform more rigourous pre-processing of the input data before being used for training and testing for a chosen estimator. \n",
    "\n",
    "Lets revisit the countries of the world dataset to illustrate these requirements. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "from sklearn.preprocessing import Imputer, LabelEncoder, OneHotEncoder, StandardScaler"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First lets read in the data from file and redo the data cleaning steps from Unit 5:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# read CSV file\n",
    "c = pd.read_csv('countries.csv', decimal=\",\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# data preprocessing\n",
    "\n",
    "# shorten feature names\n",
    "c.columns = ['Country', 'Region', 'Population', 'Area', 'Density', 'Coastline', 'Migration', 'InfantMortality', \\\n",
    "             'GDP', 'Literacy', 'Phones', 'Arable', 'Crops', 'OtherLand', 'Climate', 'Birthrate', 'Deathrate', \\\n",
    "             'Agriculture', 'Industry', 'Service']\n",
    "\n",
    "# strip all whitespace from all columns\n",
    "c = c.applymap(lambda x: x.strip() if type(x) is str else x)\n",
    "\n",
    "# set index to country\n",
    "c.set_index('Country', inplace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Suppose we wanted to use machine learning to predict the Birth rate of a country given a number of input features. We then select the features of interest and the target from the dataframe: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# select features of interest\n",
    "features = ['Region', 'Population', 'Area', 'Density', 'InfantMortality', 'GDP', 'Deathrate']\n",
    "output = ['Birthrate']\n",
    "dataset = c[features + output]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this stage there are null values in the data: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# show any null values \n",
    "dataset[dataset.isnull().any(axis=1)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It makes sense to discard any data with null values for the target: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# discard where target value is unknown\n",
    "dataset = dataset.dropna(subset=['Birthrate'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset[dataset.isnull().any(axis=1)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can we recover the data from above? One (naive) option is to replace the null values with the overall mean from each of those features (\"Infant Mortality\" and \"DeathRate\").\n",
    "\n",
    "Lets see how we can do this using methods available in Scikit Learn. First lets extract the values out of the dataframe into a data and target array:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# prepare for sklearn \n",
    "data = dataset[features].values\n",
    "target = dataset[output].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.shape, target.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we look at column 6 we now you should see the null value still present: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# show deathrate col \n",
    "data[:50, 6]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now use the `Imputer` method to replace the null value with the column mean: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# transform any missing to mean of colums\n",
    "imputer = Imputer(missing_values = 'NaN', strategy = 'mean', axis = 0)\n",
    "data[:, 2:] = imputer.fit_transform(data[:, 2:])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# null replaced by 9.24\n",
    "data[:50, 6]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The region has been chosen as an input feature:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data[1:10, 0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This will have to be **encoded** if we wish to use the feature in an ANN:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set to cat variables\n",
    "labelencoder_X = LabelEncoder()\n",
    "data[:, 0] = labelencoder_X.fit_transform(data[:, 0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data [1:10, 0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see from above that the regions have been transformed into integers (e.g EASTERN EUROPE = 3).\n",
    "\n",
    "There is still a problem however. Here the absolute value of the label is meaningless - there is no reason why 'OCEANIA' (10) is higher than 'EASTERN EUROPE' - but the machine learning algorithm may interpret this differently. To avoid this issue we create **dummy variables** using the `OneHotEncoder` method:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# dummy variables \n",
    "onehotencoder = OneHotEncoder(categorical_features = [0])\n",
    "data = onehotencoder.fit_transform(data).toarray()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## now have 1x10 variables inserted start of array \n",
    "data[1:10, 1:10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here the first column (containing the Region data) was transformed into 10 columns (one for each of the possible regions) that contain either a 0 or 1. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lastly we need to consider **feature scaling**. We can easily see that the range of values for the features can differ by orders of magnitude:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We therefore standardise all columns by applying the transformation x_stand = (x - mean(x))/std(x) using the `StandardScaler` method:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc = StandardScaler()\n",
    "data = sc.fit_transform(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example we can see that the population data now takes on the following values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## show first 5 entries of the population column \n",
    "print('\\n'.join('{}: {}'.format(*k) for k in enumerate(data[:5, 11])))\n",
    "#data[:, 11]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# max (possibly maps to China, India\n",
    "data[:,11].min(), data[:,11].max()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
