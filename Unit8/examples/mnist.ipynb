{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MNIST digit classification using ANNs in Scikit Learn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook demonstrates how Artifical Neural Networks can be used too classify the handwritten digits in the MNIST dataset.\n",
    "\n",
    "The full dataset has 60,000 training images and 10,000 testing samples encoded as a 28x28 image. Each pixel value takes a value between 0 and 255. Scikit Learn is packaged with a *reduced* dataset of 1,797 entiries of 8x8 images. Each pixel value takes a value between 0 and 16."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from sklearn import datasets, model_selection, metrics, tree, neural_network\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "import nn_utils\n",
    "%matplotlib inline\n",
    "# from __future__ import print_function # print function change if using Python 2 "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data Exploration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First lets load the digits dataset from the `datasets` module: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "digits = datasets.load_digits()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are 1,797 entries with 64 features per row: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "digits.data.shape, digits.target.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# sample entry (number 8)\n",
    "digits.data[8]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This can be visualised using a heatmap by reshaping a sample image to an 8x8 array: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_samples = len(digits.images)\n",
    "data = digits.images.reshape((n_samples, -1))\n",
    "digits.images[8] # show 8x8"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_cmap = sns.light_palette(\"Purple\", as_cmap=True)\n",
    "ax = plt.subplot()\n",
    "sns.heatmap(digits.images[8], annot=True, ax=ax, cmap=my_cmap)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`plt.imshow` can also be used to show the same image: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# show first 10 entries using imshow\n",
    "# taken from http://scikit-learn.org/stable/auto_examples/classification/plot_digits_classification.html\n",
    "images_and_labels = list(zip(digits.images, digits.target))\n",
    "for index, (image, label) in enumerate(images_and_labels[:10]):\n",
    "    plt.subplot(2, 5, index + 1)\n",
    "    plt.axis('off')\n",
    "    plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')\n",
    "    plt.title('Training: %i' % label)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### MLP classifier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To apply an ANN to the data we just follow the same proceedure we used for the decision tree. All we need to change is the name of the estimator to `neural_network.MLPClassifer`: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# train test split \n",
    "train_data, test_data, train_target, test_target = model_selection.train_test_split(\n",
    "    digits.data, digits.target, test_size=0.3, random_state=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(train_data.shape, train_target.shape, test_data.shape, test_target.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "# run classification\n",
    "clf = neural_network.MLPClassifier()\n",
    "fit = clf.fit(train_data, train_target)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets run the fit again but this time with the `Verbose` parameter set to `True`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = neural_network.MLPClassifier(verbose=True)\n",
    "fit = clf.fit(train_data, train_target)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output above should show that **over 100** training epochs (or iterations) were run and with each iteration the loss value decreased until the tolerance (the difference in loss value between successive epochs) was below **0.0001**. \n",
    "\n",
    "We can plot the evolution of loss values with training epochs: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nn_utils.lossplot(clf.loss_curve_, scale='log')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that for the training data we could reduce the number of epochs to 40 and get a similar result (note the plot above is on a log scale)\n",
    "\n",
    "As with the decision tree estimator there are hyper-parameters available to tune network construction and training:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf.get_params()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See the [Scikit learn `MLPClassifer` documentation](http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html#sklearn.neural_network.MLPClassifier)  for a detailed explanation for each of these hyper-parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Performance Evaluation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The classification performance of the ANN can be evaluated using the testing data: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "## note imbalance in training vs testing time\n",
    "# define expected and predicted \n",
    "expected = test_target\n",
    "predicted = clf.predict(test_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note the large difference in execution time between the training and testing data. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The classification report and confusion matrix show a very good performance for our chosen test sample: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "report = metrics.classification_report(expected, predicted)\n",
    "print(report)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cm = metrics.confusion_matrix(expected, predicted)\n",
    "nn_utils.heatmap(cm, labels=['True', 'Predicted'], \n",
    "        classes=[digits.target_names,digits.target_names],\n",
    "        normalize=False)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# show overall accuracy score\n",
    "clf.score(test_data, test_target)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To check consistency across the sample lets get the average accuracy using 5-fold cross validation: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time \n",
    "\n",
    "d = model_selection.train_test_split(digits.data, digits.target, test_size=0.3, random_state=0)\n",
    "clf = neural_network.MLPClassifier()\n",
    "e, p = nn_utils.runML(clf, d)\n",
    "scores = model_selection.cross_val_score(clf, digits.data, digits.target, cv=5)\n",
    "print(scores)\n",
    "print(\"Accuracy: %0.2f (+/- %0.2f)\" % (scores.mean(), scores.std() * 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see that the classifier still performs well (93-94%). We are using *stochastic* gradient descent by default (`solver=adam`) so your answer may vary - but you should see over 90%.\n",
    "\n",
    "Another cross-check we can apply is to run the fit with a sliding sample of training and testing data for a given set of hyper-parameters and split samples and then plot the results: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## feel free to alter parameters to see effect\n",
    "params = { 'max_iter' : 100 }\n",
    "nn_utils.compare_traintest(digits.data, digits.target, params=params)\n",
    "# compare_traintest(split=[0.7,0.91,0.92,0.93], params=params) # alternative - provide list of training/test split values\n",
    "plt.show()    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the training sample is very robust with respect to sample size and that we only start to significantly drop in testing accuracy when less than 10% of the sample is reserved for training. Most real world use cases will not exhibit this pattern.\n",
    "\n",
    "Finally in our evaluation we can perform a grid search to see the effect of modifying the number of neurons in the hidden layer:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "# parameter scan\n",
    "# Note \n",
    "# - expect convergence warnings for small networks \n",
    "# - takes ~2-3 minutes to complete\n",
    "params = {'hidden_layer_sizes' : [(5,), (10,), (20,), (50,), (100,), (200,), (500,), (1000,)]}\n",
    "score = 'accuracy'\n",
    "clf = model_selection.GridSearchCV(neural_network.MLPClassifier(), params, cv=5, scoring='%s' % score)\n",
    "clf.fit(digits.data, digits.target)\n",
    "\n",
    "print(clf.best_params_)\n",
    "means = clf.cv_results_['mean_test_score']\n",
    "stds = clf.cv_results_['std_test_score']\n",
    "\n",
    "for mean, std, param in zip(means, stds, clf.cv_results_['params']):\n",
    "        print(\"%0.3f (+/-%0.03f) for %r\"\n",
    "              % (mean, std * 2, param))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I found that the largest choice - 1,000 hidden layer nodes - was the best choice (your results may differ). The temptation here would be to keep increasing the layer size by a larger amounts. However this has to be balanced against the fact that it takes roughly **10x** longer for a **1%** accuracy improvement between 500 and 1,000 hidden nodes. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Coefficients and network output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets re-run the fit and prediction to extract more details from our ANN:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = neural_network.MLPClassifier()\n",
    "d = model_selection.train_test_split(digits.data, digits.target, test_size=0.7, random_state=0)\n",
    "e, p = nn_utils.runML(clf, d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The synapse weights can be extracted from `clf.coefs_`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get coef of weights\n",
    "[coef.shape for coef in clf.coefs_]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This provides us with two weight matricies:\n",
    "- 64 x 100 weights connecting the input layer to the hidden layer \n",
    "- 100 x 10 weights connecting the hidden layer to the output layer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then visualise the weights of the 64 connections to the first N neurons of the hidden layer (where N is set to 10):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# adapted from http://scikit-learn.org/stable/auto_examples/neural_networks/plot_mnist_filters.html#sphx-glr-auto-examples-neural-networks-plot-mnist-filters-py\n",
    "N = 10\n",
    "fig, axes = plt.subplots(1,N)\n",
    "\n",
    "# use global min / max to ensure all weights are shown on the same scale\n",
    "vmin, vmax = clf.coefs_[0].min(), clf.coefs_[0].max()\n",
    "\n",
    "for coef, ax in zip(fit.coefs_[0].T, axes.ravel()):\n",
    "    ax.matshow(coef.reshape(8, 8), cmap=plt.cm.gray, vmin=.5 * vmin,\n",
    "               vmax=.5 * vmax)\n",
    "    ax.set_xticks(())\n",
    "    ax.set_yticks(())\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Do not worry if you cannot make sense of the above plots! In some cases patterns are seen (or even residual images reflecting the input data) but for the most part all that will be seen is \"noise\". All that this really illustrates is that ANNs are a \"black box\" and harder to interpret than \"white box\" methods such as Decision Trees. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally take a look at the output layer to see how classification decisions are made: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get predictions for first 10 digits\n",
    "probs = np.array(clf.predict_proba(digits.data[:10]))\n",
    "\n",
    "# zero any probabilties less than 0.01 and round off to 2 decimal places \n",
    "probs[probs < 0.01] = 0\n",
    "probs = np.around(probs, decimals=2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# show on heatmap\n",
    "nn_utils.heatmap(probs, classes=[digits.target_names,digits.target_names], labels=['True', 'Predicted'], palette=\"Green\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The heatmap above illustrates that for the first 10 digits in the sample there is near certainty predicting the correct value apart from the number \"5\". Here the ANN predicts the number \"5\" in the sample as the number \"9\" or the number \"3\" (remember this is a stochastic process so the answers each time may be different). Lets have a look at the image to see why: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# represent sample entry as 8x8 heat map\n",
    "my_cmap = sns.light_palette(\"Purple\", as_cmap=True)\n",
    "ax = plt.subplot()\n",
    "sns.heatmap(digits.images[5], annot=True, ax=ax, cmap=my_cmap)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tricky! What would you say if you did not already know this was a number \"5\"? "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
